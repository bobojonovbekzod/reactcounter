import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  const [count, setCount] = useState(0);

  const increase = () =>{
    setCount(count+1)
  }
  const decrease = () =>{
    setCount(count-1)
  }
  const zero = () =>{
    setCount(0)
  }

  return (
    <div className='bg-indigo-300 h-screen flex flex-col justify-center items-center'>
        <h1 className='text-2xl font-semibold mb-5'>Counter</h1>
        <p className='font-medium text-2xl'>{count}</p>
        <div> 
          <button onClick={increase} className='bg-orange-300 m-3 p-2 rounded font-medium'>Increase ++</button>
          <button onClick={decrease} className='bg-orange-300 m-3 p-2 rounded font-medium'>Decrease --</button>
          <button onClick={zero} className='bg-orange-300 m-3 p-2 rounded font-medium'>Zero 0</button>
        </div>
    </div>
  )
}

export default App
